/*
 * file: final_interface.v
 * This final combines both interface and control of the SRAM (SPI protocol)
 */

// instantiate interface and control

module final_interface(sdo, sck, sdi, ss, clk, reset);

output sdo;

input sck;
input sdi;
input ss;
input clk;
input reset;

// prefix "sc" means sram to controller
wire sc_wr_out;
wire[2:0] sc_count_out;

// prefix "cs" means controller to sram
wire cs_shft_addr;
wire cs_shft_data;
wire cs_wr_load;
wire cs_enable;
wire cs_shft_out;
wire cs_out_load;
wire cs_count;

sram_interface sramInterface(.sdo(sdo),
							 .wr_out(sc_wr_out),
							 .count_out(sc_count_out),
							 .sck(sck),
							 .sdi(sdi),
							 .shft_addr(cs_shft_addr),
							 .shft_data(cs_shft_data),
							 .wr_load(cs_wr_load),
							 .enable(cs_enable),
							 .shft_out(cs_shft_out),
							 .out_load(cs_out_load),
							 .reset(reset),
							 .clk(clk),
							 .count(cs_count));

controller sramController(.shft_addr(cs_shft_addr),
						  .shft_data(cs_shft_data),
						  .wr_load(cs_wr_load),
						  .enable(cs_enable),
						  .shft_out(cs_shft_out),
						  .out_load(cs_out_load),
						  .count(cs_count),
						  .ss(ss),
						  .sck(sck),
						  .reset(reset),
						  .wr_out(sc_wr_out),
						  .count_out(sc_count_out));

endmodule