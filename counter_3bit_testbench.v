/*
 * file: counter_3bit_testbench
 * This file tests the functions of the 3bit counter
 */

module counter_3bit_testbench;

//inputs
reg t_clk, t_reset, t_count;

//outputs
wire[2:0] t_out;

//instantiation
counter_3bit count3bit(.out(t_out),
					   .clk(t_clk),
					   .reset(t_reset),
					   .count(t_count));
initial
begin
//initialization
	t_clk = 0;
	t_reset = 1;
	t_count = 0;
end

always #100 t_clk = ~t_clk;

initial
begin
	#150 t_reset = 0; t_count = 1;
end

endmodule
