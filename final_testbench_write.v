
/*
 * file: final_testbench_write
 * This is the final testbench program to check the whole SPI-SRAM functions
 * This will write 0 to 255 in SRAM with data 0 to 255 in decimal
 */

module final_testbench_write;

// inputs
reg t_sck;
reg t_sdi;
reg t_ss;
reg t_clk;
reg t_reset;

// outputs
wire t_sdo;

// instantiation
final_interface finalInterface(.sdo(t_sdo),
							   .sck(t_sck),
							   .sdi(t_sdi),
							   .ss(t_ss),
							   .clk(t_clk),
							   .reset(t_reset));

initial
begin
	// initialization
	t_sck = 0;
	t_ss = 1;
	t_clk = 0;
	t_reset = 0;
end

always #80 t_sck = ~t_sck;
always #10 t_clk = ~t_clk;

initial
begin
	#40 t_reset = 1;
	#80 t_reset = 0; t_ss = 0;
	// MEM[0] = 0
	#40 t_sdi = 0; // A[7] = 0; @160ns
	#160 t_sdi = 0; // A[6] = 0;
	#160 t_sdi = 0; // A[5] = 0;
	#160 t_sdi = 0; // A[4] = 0;
	#160 t_sdi = 0; // A[3] = 0;
	#160 t_sdi = 0; // A[2] = 0;
	#160 t_sdi = 0; // A[1] = 0;
	#160 t_sdi = 0; // A[0] = 0;
	// Address[7:0] = 8b'00000000
	#160 t_sdi = 0; // Write
	#160 t_sdi = 0; // D[7] = 0;
	#160 t_sdi = 0; // D[6] = 0;
	#160 t_sdi = 0; // D[5] = 0;
	#160 t_sdi = 0; // D[4] = 0;
	#160 t_sdi = 0; // D[3] = 0;
	#160 t_sdi = 0; // D[2] = 0;
	#160 t_sdi = 0; // D[1] = 0;
	#160 t_sdi = 0; // D[0] = 0;
	#160 // write enable time
	// MEM[1] = 1
	#160 t_sdi = 0; // A[7] = 0;
	#160 t_sdi = 0; // A[6] = 0;
	#160 t_sdi = 0; // A[5] = 0;
	#160 t_sdi = 0; // A[4] = 0;
	#160 t_sdi = 0; // A[3] = 0;
	#160 t_sdi = 0; // A[2] = 0;
	#160 t_sdi = 0; // A[1] = 0;
	#160 t_sdi = 1; // A[0] = 1;
	// Address[7:0] = 8b'00000000
	#160 t_sdi = 0; // Write
	#160 t_sdi = 0; // D[7] = 0;
	#160 t_sdi = 0; // D[6] = 0;
	#160 t_sdi = 0; // D[5] = 0;
	#160 t_sdi = 0; // D[4] = 0;
	#160 t_sdi = 0; // D[3] = 0;
	#160 t_sdi = 0; // D[2] = 0;
	#160 t_sdi = 0; // D[1] = 0;
	#160 t_sdi = 1; // D[0] = 1;
	#160 // write enable time
	// MEM[2] = 2
	#160 t_sdi = 0; // A[7] = 0;
	#160 t_sdi = 0; // A[6] = 0;
	#160 t_sdi = 0; // A[5] = 0;
	#160 t_sdi = 0; // A[4] = 0;
	#160 t_sdi = 0; // A[3] = 0;
	#160 t_sdi = 0; // A[2] = 0;
	#160 t_sdi = 1; // A[1] = 1;
	#160 t_sdi = 0; // A[0] = 0;
	// Address[7:0] = 8b'00000000
	#160 t_sdi = 0; // Write
	#160 t_sdi = 0; // D[7] = 0;
	#160 t_sdi = 0; // D[6] = 0;
	#160 t_sdi = 0; // D[5] = 0;
	#160 t_sdi = 0; // D[4] = 0;
	#160 t_sdi = 0; // D[3] = 0;
	#160 t_sdi = 0; // D[2] = 0;
	#160 t_sdi = 1; // D[1] = 1;
	#160 t_sdi = 0; // D[0] = 0;
	#160 // write enable time
	// MEM[3] = 3
	#160 t_sdi = 0; // A[7] = 0;
	#160 t_sdi = 0; // A[6] = 0;
	#160 t_sdi = 0; // A[5] = 0;
	#160 t_sdi = 0; // A[4] = 0;
	#160 t_sdi = 0; // A[3] = 0;
	#160 t_sdi = 0; // A[2] = 0;
	#160 t_sdi = 1; // A[1] = 1;
	#160 t_sdi = 1; // A[0] = 1;
	// Address[7:0] = 8b'00000000
	#160 t_sdi = 0; // Write
	#160 t_sdi = 0; // D[7] = 0;
	#160 t_sdi = 0; // D[6] = 0;
	#160 t_sdi = 0; // D[5] = 0;
	#160 t_sdi = 0; // D[4] = 0;
	#160 t_sdi = 0; // D[3] = 0;
	#160 t_sdi = 0; // D[2] = 0;
	#160 t_sdi = 1; // D[1] = 1;
	#160 t_sdi = 1; // D[0] = 1;
	#160 t_sdi = 0;// write enable time
	// MEM[4] = 4
	#160 t_sdi = 0; // A[7]
	#160 t_sdi = 0; // A[6]
	#160 t_sdi = 0; // A[5]
	#160 t_sdi = 0; // A[4]
	#160 t_sdi = 0; // A[3]
	#160 t_sdi = 1; // A[2]
	#160 t_sdi = 0; // A[1]
	#160 t_sdi = 0; // A[0]
	#160 t_sdi = 0; // Write
	#160 t_sdi = 0; // D[7]
	#160 t_sdi = 0; // D[6]
	#160 t_sdi = 0; // D[5]
	#160 t_sdi = 0; // D[4]
	#160 t_sdi = 0; // D[3]
	#160 t_sdi = 1; // D[2]
	#160 t_sdi = 0; // D[1]
	#160 t_sdi = 0; // D[0]
	#160 t_sdi = 0;// write enable time
	// MEM[5] = 5
	#160 t_sdi = 0; // A[7]
	#160 t_sdi = 0; // A[6]
	#160 t_sdi = 0; // A[5]
	#160 t_sdi = 0; // A[4]
	#160 t_sdi = 0; // A[3]
	#160 t_sdi = 1; // A[2]
	#160 t_sdi = 0; // A[1]
	#160 t_sdi = 1; // A[0]
	#160 t_sdi = 0; // Write
	#160 t_sdi = 0; // D[7]
	#160 t_sdi = 0; // D[6]
	#160 t_sdi = 0; // D[5]
	#160 t_sdi = 0; // D[4]
	#160 t_sdi = 0; // D[3]
	#160 t_sdi = 1; // D[2]
	#160 t_sdi = 0; // D[1]
	#160 t_sdi = 1; // D[0]
	#160 t_sdi = 0;// write enable time
	// MEM[6] = 6
	#160 t_sdi = 0; // A[7]
	#160 t_sdi = 0; // A[6]
	#160 t_sdi = 0; // A[5]
	#160 t_sdi = 0; // A[4]
	#160 t_sdi = 0; // A[3]
	#160 t_sdi = 1; // A[2]
	#160 t_sdi = 1; // A[1]
	#160 t_sdi = 0; // A[0]
	#160 t_sdi = 0; // Write
	#160 t_sdi = 0; // D[7]
	#160 t_sdi = 0; // D[6]
	#160 t_sdi = 0; // D[5]
	#160 t_sdi = 0; // D[4]
	#160 t_sdi = 0; // D[3]
	#160 t_sdi = 1; // D[2]
	#160 t_sdi = 1; // D[1]
	#160 t_sdi = 0; // D[0]
	#160 t_sdi = 0;// write enable time
	// MEM[7] = 7
	#160 t_sdi = 0; // A[7]
	#160 t_sdi = 0; // A[6]
	#160 t_sdi = 0; // A[5]
	#160 t_sdi = 0; // A[4]
	#160 t_sdi = 0; // A[3]
	#160 t_sdi = 1; // A[2]
	#160 t_sdi = 1; // A[1]
	#160 t_sdi = 1; // A[0]
	#160 t_sdi = 0; // Write
	#160 t_sdi = 0; // D[7]
	#160 t_sdi = 0; // D[6]
	#160 t_sdi = 0; // D[5]
	#160 t_sdi = 0; // D[4]
	#160 t_sdi = 0; // D[3]
	#160 t_sdi = 1; // D[2]
	#160 t_sdi = 1; // D[1]
	#160 t_sdi = 1; // D[0]
	#160 t_sdi = 0;// write enable time
	// MEM[8] = 8
	#160 t_sdi = 0; // A[7]
	#160 t_sdi = 0; // A[6]
	#160 t_sdi = 0; // A[5]
	#160 t_sdi = 0; // A[4]
	#160 t_sdi = 1; // A[3]
	#160 t_sdi = 0; // A[2]
	#160 t_sdi = 0; // A[1]
	#160 t_sdi = 0; // A[0]
	#160 t_sdi = 0; // Write
	#160 t_sdi = 0; // D[7]
	#160 t_sdi = 0; // D[6]
	#160 t_sdi = 0; // D[5]
	#160 t_sdi = 0; // D[4]
	#160 t_sdi = 1; // D[3]
	#160 t_sdi = 0; // D[2]
	#160 t_sdi = 0; // D[1]
	#160 t_sdi = 0; // D[0]
	#160 t_sdi = 0;// write enable time
	// MEM[9] = 9
	#160 t_sdi = 0; // A[7]
	#160 t_sdi = 0; // A[6]
	#160 t_sdi = 0; // A[5]
	#160 t_sdi = 0; // A[4]
	#160 t_sdi = 1; // A[3]
	#160 t_sdi = 0; // A[2]
	#160 t_sdi = 0; // A[1]
	#160 t_sdi = 1; // A[0]
	#160 t_sdi = 0; // Write
	#160 t_sdi = 0; // D[7]
	#160 t_sdi = 0; // D[6]
	#160 t_sdi = 0; // D[5]
	#160 t_sdi = 0; // D[4]
	#160 t_sdi = 1; // D[3]
	#160 t_sdi = 0; // D[2]
	#160 t_sdi = 0; // D[1]
	#160 t_sdi = 1; // D[0]
	#160 t_sdi = 0;// write enable time
	// MEM[10] = 10
	#160 t_sdi = 0; // A[7]
	#160 t_sdi = 0; // A[6]
	#160 t_sdi = 0; // A[5]
	#160 t_sdi = 0; // A[4]
	#160 t_sdi = 1; // A[3]
	#160 t_sdi = 0; // A[2]
	#160 t_sdi = 1; // A[1]
	#160 t_sdi = 0; // A[0]
	#160 t_sdi = 0; // Write
	#160 t_sdi = 0; // D[7]
	#160 t_sdi = 0; // D[6]
	#160 t_sdi = 0; // D[5]
	#160 t_sdi = 0; // D[4]
	#160 t_sdi = 1; // D[3]
	#160 t_sdi = 0; // D[2]
	#160 t_sdi = 1; // D[1]
	#160 t_sdi = 0; // D[0]
	#160 t_sdi = 0;// write enable time
	// MEM[250] = 250
	#160 t_sdi = 1; // A[7]
	#160 t_sdi = 1; // A[6]
	#160 t_sdi = 1; // A[5]
	#160 t_sdi = 1; // A[4]
	#160 t_sdi = 1; // A[3]
	#160 t_sdi = 0; // A[2]
	#160 t_sdi = 1; // A[1]
	#160 t_sdi = 0; // A[0]
	#160 t_sdi = 0; // Write
	#160 t_sdi = 1; // D[7]
	#160 t_sdi = 1; // D[6]
	#160 t_sdi = 1; // D[5]
	#160 t_sdi = 1; // D[4]
	#160 t_sdi = 1; // D[3]
	#160 t_sdi = 0; // D[2]
	#160 t_sdi = 1; // D[1]
	#160 t_sdi = 0; // D[0]
	#160 t_sdi = 0;// write enable time
	// MEM[251] = 251
	#160 t_sdi = 1; // A[7]
	#160 t_sdi = 1; // A[6]
	#160 t_sdi = 1; // A[5]
	#160 t_sdi = 1; // A[4]
	#160 t_sdi = 1; // A[3]
	#160 t_sdi = 0; // A[2]
	#160 t_sdi = 1; // A[1]
	#160 t_sdi = 1; // A[0]
	#160 t_sdi = 0; // Write
	#160 t_sdi = 1; // D[7]
	#160 t_sdi = 1; // D[6]
	#160 t_sdi = 1; // D[5]
	#160 t_sdi = 1; // D[4]
	#160 t_sdi = 1; // D[3]
	#160 t_sdi = 0; // D[2]
	#160 t_sdi = 1; // D[1]
	#160 t_sdi = 1; // D[0]
	#160 t_sdi = 0;// write enable time
	// MEM[252] = 252
	#160 t_sdi = 1; // A[7]
	#160 t_sdi = 1; // A[6]
	#160 t_sdi = 1; // A[5]
	#160 t_sdi = 1; // A[4]
	#160 t_sdi = 1; // A[3]
	#160 t_sdi = 1; // A[2]
	#160 t_sdi = 0; // A[1]
	#160 t_sdi = 0; // A[0]
	#160 t_sdi = 0; // Write
	#160 t_sdi = 1; // D[7]
	#160 t_sdi = 1; // D[6]
	#160 t_sdi = 1; // D[5]
	#160 t_sdi = 1; // D[4]
	#160 t_sdi = 1; // D[3]
	#160 t_sdi = 1; // D[2]
	#160 t_sdi = 0; // D[1]
	#160 t_sdi = 0; // D[0]
	#160 t_sdi = 0;// write enable time
	// MEM[253] = 253
	#160 t_sdi = 1; // A[7]
	#160 t_sdi = 1; // A[6]
	#160 t_sdi = 1; // A[5]
	#160 t_sdi = 1; // A[4]
	#160 t_sdi = 1; // A[3]
	#160 t_sdi = 1; // A[2]
	#160 t_sdi = 0; // A[1]
	#160 t_sdi = 1; // A[0]
	#160 t_sdi = 0; // Write
	#160 t_sdi = 1; // D[7]
	#160 t_sdi = 1; // D[6]
	#160 t_sdi = 1; // D[5]
	#160 t_sdi = 1; // D[4]
	#160 t_sdi = 1; // D[3]
	#160 t_sdi = 1; // D[2]
	#160 t_sdi = 0; // D[1]
	#160 t_sdi = 1; // D[0]
	#160 t_sdi = 0;// write enable time
	// MEM[254] = 254
	#160 t_sdi = 1; // A[7]
	#160 t_sdi = 1; // A[6]
	#160 t_sdi = 1; // A[5]
	#160 t_sdi = 1; // A[4]
	#160 t_sdi = 1; // A[3]
	#160 t_sdi = 1; // A[2]
	#160 t_sdi = 1; // A[1]
	#160 t_sdi = 0; // A[0]
	#160 t_sdi = 0; // Write
	#160 t_sdi = 1; // D[7]
	#160 t_sdi = 1; // D[6]
	#160 t_sdi = 1; // D[5]
	#160 t_sdi = 1; // D[4]
	#160 t_sdi = 1; // D[3]
	#160 t_sdi = 1; // D[2]
	#160 t_sdi = 1; // D[1]
	#160 t_sdi = 0; // D[0]
	#160 t_sdi = 0;// write enable time
	// MEM[255] = 255
	#160 t_sdi = 1; // A[7]
	#160 t_sdi = 1; // A[6]
	#160 t_sdi = 1; // A[5]
	#160 t_sdi = 1; // A[4]
	#160 t_sdi = 1; // A[3]
	#160 t_sdi = 1; // A[2]
	#160 t_sdi = 1; // A[1]
	#160 t_sdi = 1; // A[0]
	#160 t_sdi = 0; // Write
	#160 t_sdi = 1; // D[7]
	#160 t_sdi = 1; // D[6]
	#160 t_sdi = 1; // D[5]
	#160 t_sdi = 1; // D[4]
	#160 t_sdi = 1; // D[3]
	#160 t_sdi = 1; // D[2]
	#160 t_sdi = 1; // D[1]
	#160 t_sdi = 1; // D[0]
	#160 t_sdi = 0;// write enable time
end

endmodule
