/*
 * file: final_interface_testbench.v
 * This is the final testbench program to check the whole SPI-SRAM functions
 */

module final_interface_testbench;

// inputs
reg t_sck;
reg t_sdi;
reg t_ss;
reg t_clk;
reg t_reset;

// outputs
wire t_sdo;

// instantiation
final_interface finalInterface(.sdo(t_sdo),
							   .sck(t_sck),
							   .sdi(t_sdi),
							   .ss(t_ss),
							   .clk(t_clk),
							   .reset(t_reset));

initial
begin
	// initialization
	t_sck = 0;
	t_ss = 1;
	t_clk = 0;
	t_reset = 0;
end

always #400 t_sck = ~t_sck;
always #50 t_clk = ~t_clk;

initial
begin
	#200 t_reset = 1;
	#400 t_reset = 0; t_ss = 0;
	#200 t_sdi = 1; // A[7] = 1; @160ns
	#800 t_sdi = 0; // A[6] = 0;
	#800 t_sdi = 1; // A[5] = 1;
	#800 t_sdi = 0; // A[4] = 0;
	#800 t_sdi = 1; // A[3] = 1;
	#800 t_sdi = 0; // A[2] = 0;
	#800 t_sdi = 1; // A[1] = 1;
	#800 t_sdi = 0; // A[0] = 0;
	// Address[7:0] = 8b'10101010
	#800 t_sdi = 0; // Write
	#800 t_sdi = 0; // D[7] = 0;
	#800 t_sdi = 1; // D[6] = 1;
	#800 t_sdi = 0; // D[5] = 0;
	#800 t_sdi = 1; // D[4] = 1;
	#800 t_sdi = 0; // D[3] = 0;
	#800 t_sdi = 1; // D[2] = 1;
	#800 t_sdi = 0; // D[1] = 0;
	#800 t_sdi = 1; // D[0] = 1;
	#800 // write enable time
	// Data[7:0] = 8b'01010101
	#800 t_sdi = 1; // A[7] = 1
	#800 t_sdi = 0; // A[6] = 0;
	#800 t_sdi = 1; // A[5] = 1;
	#800 t_sdi = 0; // A[4] = 0;
	#800 t_sdi = 1; // A[3] = 1;
	#800 t_sdi = 0; // A[2] = 0;
	#800 t_sdi = 1; // A[1] = 1;
	#800 t_sdi = 0; // A[0] = 0;
	// Address[7:0] = 8b'10101010
	#800 t_sdi = 1; // Read
end

endmodule
