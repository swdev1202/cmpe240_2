/*
 * file: shft_out_reg.v
 * This is a shift register that outputs a bit at a time and shifts.
 */

module shft_out_reg(shft_out, in, clk, reset, load, shift);

/* outputs */
output shft_out; // 1-bit

/* inputs */
input[7:0] in;
input clk;
input reset;
input load;
input shift;

reg[7:0] data;

assign shft_out = data[7];

always @(posedge clk)
begin
	if(reset) begin
		data <= 0;
	end
	else begin
		if(load) begin
			data <= in;
		end
		else begin
			if(shift) begin
				//shft_out <= data[7]; // sending out the MSB
				data = data << 1;
			end
		end
	end
end

endmodule