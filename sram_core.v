/*
 * file : sram_core.v 
 *
 */
module sram_core(dout, din, addr, clk, we, re, reset);

output reg[7:0] dout;

input clk, we, re, reset;
input[7:0] din;
input[7:0] addr; /* 0-255 */

reg[7:0] sram[255:0]; /* 0 - 255 */

integer i;

always @ (posedge clk)
begin
	if(reset == 1) begin
		for(i=0; i<256; i=i+1) begin
			sram[i] <= 0;
		end
	end
	else begin
		if(we) begin
			sram[addr] <= din;
		end
		if(re) begin
			dout <= sram[addr];
		end
	end
end

endmodule