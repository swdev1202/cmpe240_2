/*
 * file: ff_testbench.v
 * This is a testbench program to test the functions of ff (ff.v)
 */

module ff_testbench;

// inputs
reg t_d;
reg t_clk, t_enable, t_reset;

// outputs
wire t_q;

// instantiation
ff flipflop(.q(t_q),
			.d(t_d),
			.clk(t_clk),
			.enable(t_enable),
			.reset(t_reset));
initial
begin
// initialization
	t_clk = 0;
	t_enable = 0;
	t_reset = 1;
end

always #100 t_clk = ~t_clk;

// positive edge clk every 100, 300, 500, 700, ... ns

initial
begin
	#150 t_reset = 0; t_enable = 1; // no more reset signal @150 ns
	#100 t_d = 1; // t_d signal up @250 ns
	#200 t_d = 0;
	#200 t_d = 1;
	#200 t_reset = 1;
end

endmodule