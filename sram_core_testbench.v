/*
 * file : sram_core_testbench.v
 * This is the testbench program for sram_core
 */

module sram_core_testbench;

wire[7:0] t_dout;

reg t_clk;
reg t_we;
reg t_re;
reg t_reset;
reg[7:0] t_din;
reg[7:0] t_addr;

sram_core sram(.dout(t_dout),
			   .din(t_din),
			   .addr(t_addr),
			   .clk(t_clk),
			   .we(t_we),
			   .re(t_re),
			   .reset(t_reset));


initial begin
	t_clk = 0;
	t_reset = 1;
	t_we = 0;
	t_re = 0;
end

always #100 t_clk = ~t_clk;

initial begin
	#150 t_reset = 0; // @ 150 ns
	#100 t_addr = 10; t_din = 11; t_we = 1; // @ 250ns
	// @ 300 ns, clock hits posedge; MEM[10] = 11
	#200 t_addr = 50; t_din = 51; // @ 450ns
	// @ 500 ns, clock hits posedge; MEM[50] = 51
	#200 t_addr = 75; t_din = 76; // @ 650ns
	// @ 700 ns, clock hits posedge; MEM[75] = 76
	#200 t_addr = 255; t_din = 200; // @ 850ns
	// @ 900 ns, clock hits posedge; MEM[255] = 200
	
	#200 t_addr = 10; t_we = 0; t_re = 1; // @ 1050ns
	// @ 1100ns, clock hits posedge; read MEM[10] -> 11
	#200 t_addr = 50; // @ 1250ns
	// @ 1300ns, clock hits posedge; read MEM[50] -> 51
	#200 t_addr = 75; // @ 1450ns
	// @ 1500ns, clock hits posedge; read MEM[75] -> 76
	#200 t_addr = 255; // @ 1650ns
	// @ 1700ns, clock hits posedge; read MEM[255] -> 200
	#200 t_addr = 111; // @ 1850ns
	// @ 1900ns, clock hits posedge; read MEM[111] -> 0
	/*
	#200 t_reset = 1; // @ 2050ns
	// @ 2100ns, clock hits posedge; reset all
	#200 t_addr = 50; // @ 2250ns
	// @ 2300ns, clock hits posedge; read MEM[50] -> 0*/
end

endmodule
	
	
	
	
	

	