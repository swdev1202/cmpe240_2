/*
 * file: ff.v
 * This is a positive edge triggered 1-bit flip flop with a reset signal.
 */

module ff(q, d, clk, enable, reset);
output q;
input d;
input clk, enable, reset;

reg q;

always @(posedge clk)
begin
	if(reset)
	begin
	    q <= 0;
	end
	else
	begin
		if(enable)
		begin
			q <= d;
		end
	end
end

endmodule