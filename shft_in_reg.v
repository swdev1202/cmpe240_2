/*
 * file: shft_in_reg.v
 * This is a shift register that takes a bit at a time and shifts.
 */

module shft_in_reg(out, shft_in, clk, reset, shift);

/* outputs */
output[7:0] out;

/* inputs */
input shft_in;	// 1-bit
input clk;
input reset;
input shift;

reg[7:0] data;

assign out = data;

always @(posedge clk)
begin
	if(reset) begin
		data <= 0;
	end
	else begin
		if(shift) begin
			data = {data[6:0], shft_in};
			//data <= data[6:0] + shft_in; // data = D6...0 | IN (MSB)
		end
	end
end

endmodule