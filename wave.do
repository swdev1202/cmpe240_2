onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate -label COUNT_OUT -radix unsigned /final_interface_testbench/finalInterface/sramController/count_out
add wave -noupdate -label CONTROL_STATE -radix unsigned /final_interface_testbench/finalInterface/sramController/state
add wave -noupdate -group FINAL -label SRAM_CLK -radix binary /final_interface_testbench/t_clk
add wave -noupdate -group FINAL -label SCK -radix binary /final_interface_testbench/t_sck
add wave -noupdate -group FINAL -label RESET -radix binary /final_interface_testbench/t_reset
add wave -noupdate -group FINAL -label SS -radix binary /final_interface_testbench/t_ss
add wave -noupdate -group FINAL -label SDI -radix binary /final_interface_testbench/t_sdi
add wave -noupdate -group FINAL -label SDO -radix binary /final_interface_testbench/t_sdo
add wave -noupdate -group FINAL -label WR_OUT -radix binary /final_interface_testbench/finalInterface/sramController/wr_out
add wave -noupdate -group ADDR_REG -label ADDR_REG_SHIFT -radix binary /final_interface_testbench/finalInterface/sramInterface/addr_reg/shift
add wave -noupdate -group ADDR_REG -label ADDR_REG_SHIFT_IN -radix binary /final_interface_testbench/finalInterface/sramInterface/addr_reg/shft_in
add wave -noupdate -group ADDR_REG -label ADDR_REG_OUT -radix binary /final_interface_testbench/finalInterface/sramInterface/addr_reg/out
add wave -noupdate -group DATA_REG -label DATA_REG_SHIFT -radix binary /final_interface_testbench/finalInterface/sramInterface/data_reg/shift
add wave -noupdate -group DATA_REG -label DATA_REG_SHIFT_IN -radix binary /final_interface_testbench/finalInterface/sramInterface/data_reg/shft_in
add wave -noupdate -group DATA_REG -label DATA_REG_OUT -radix binary /final_interface_testbench/finalInterface/sramInterface/data_reg/out
add wave -noupdate -group OUT_REG -label SRAM_OUT_REG_LOAD -radix binary /final_interface_testbench/finalInterface/sramInterface/out_reg/load
add wave -noupdate -group OUT_REG -label SRAM_OUT_REG_SHIFT -radix binary /final_interface_testbench/finalInterface/sramInterface/out_reg/shift
add wave -noupdate -group OUT_REG -label SRAM_OUT_REG_DATAIN -radix binary -childformat {{{/final_interface_testbench/finalInterface/sramInterface/out_reg/in[7]} -radix binary} {{/final_interface_testbench/finalInterface/sramInterface/out_reg/in[6]} -radix binary} {{/final_interface_testbench/finalInterface/sramInterface/out_reg/in[5]} -radix binary} {{/final_interface_testbench/finalInterface/sramInterface/out_reg/in[4]} -radix binary} {{/final_interface_testbench/finalInterface/sramInterface/out_reg/in[3]} -radix binary} {{/final_interface_testbench/finalInterface/sramInterface/out_reg/in[2]} -radix binary} {{/final_interface_testbench/finalInterface/sramInterface/out_reg/in[1]} -radix binary} {{/final_interface_testbench/finalInterface/sramInterface/out_reg/in[0]} -radix binary}} -subitemconfig {{/final_interface_testbench/finalInterface/sramInterface/out_reg/in[7]} {-height 15 -radix binary} {/final_interface_testbench/finalInterface/sramInterface/out_reg/in[6]} {-height 15 -radix binary} {/final_interface_testbench/finalInterface/sramInterface/out_reg/in[5]} {-height 15 -radix binary} {/final_interface_testbench/finalInterface/sramInterface/out_reg/in[4]} {-height 15 -radix binary} {/final_interface_testbench/finalInterface/sramInterface/out_reg/in[3]} {-height 15 -radix binary} {/final_interface_testbench/finalInterface/sramInterface/out_reg/in[2]} {-height 15 -radix binary} {/final_interface_testbench/finalInterface/sramInterface/out_reg/in[1]} {-height 15 -radix binary} {/final_interface_testbench/finalInterface/sramInterface/out_reg/in[0]} {-height 15 -radix binary}} /final_interface_testbench/finalInterface/sramInterface/out_reg/in
add wave -noupdate -group OUT_REG -label SRAM_OUT_REG_DATA -radix binary /final_interface_testbench/finalInterface/sramInterface/out_reg/data
add wave -noupdate -group OUT_REG -label SRAM_OUT_REG_DATAOUT -radix binary /final_interface_testbench/finalInterface/sramInterface/out_reg/shft_out
add wave -noupdate -group WR_REG -label W/R_OUT -radix binary /final_interface_testbench/finalInterface/sramInterface/wr_ff/q
add wave -noupdate -group WR_REG -label W/R_ENABLE -radix binary /final_interface_testbench/finalInterface/sramInterface/wr_ff/enable
add wave -noupdate -expand -group SRAM -label SRAM_CLK -radix binary /final_interface_testbench/finalInterface/sramInterface/_sram_core/clk
add wave -noupdate -expand -group SRAM -label SRAM_RESET -radix binary /final_interface_testbench/finalInterface/sramInterface/_sram_core/reset
add wave -noupdate -expand -group SRAM -label SRAM_WE -radix binary /final_interface_testbench/finalInterface/sramInterface/_sram_core/we
add wave -noupdate -expand -group SRAM -label SRAM_RE -radix binary /final_interface_testbench/finalInterface/sramInterface/_sram_core/re
add wave -noupdate -expand -group SRAM -label SRAM_ADDR -radix binary /final_interface_testbench/finalInterface/sramInterface/_sram_core/addr
add wave -noupdate -expand -group SRAM -label SRAM_DATAIN -radix binary /final_interface_testbench/finalInterface/sramInterface/_sram_core/din
add wave -noupdate -expand -group SRAM -label SRAM_DATAOUT -radix binary /final_interface_testbench/finalInterface/sramInterface/_sram_core/dout
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {22888 ns} 0}
quietly wave cursor active 1
configure wave -namecolwidth 205
configure wave -valuecolwidth 123
configure wave -justifyvalue left
configure wave -signalnamewidth 0
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ns
update
WaveRestoreZoom {17886 ns} {27890 ns}
