/*
 * file: shft_in_reg_testbench.v 
 * This is a testbench program to test the shift_in register (shft_in_reg.v)
 */

module shft_in_reg_testbench;

//inputs
reg t_shft_in;
reg t_clk;
reg t_reset;
reg t_shift;

//outputs
wire[7:0] t_out;

// instantiation
shft_in_reg shifInReg(.out(t_out),
					  .shft_in(t_shft_in),
					  .clk(t_clk),
					  .reset(t_reset),
					  .shift(t_shift));
initial
begin
	// initialize
	t_shift = 0;
	t_reset = 1;
	t_clk = 0;
end

always #100 t_clk = ~t_clk;

initial
begin
	#150 t_reset = 0; // @ 150 ns
	#100 t_shft_in = 1; t_shift = 1; // @ 250 ns
	// @ 300 ns, clock hits posedge; regVal = 00000001
	#200 t_shft_in = 0; // @ 450 ns
	// @ 500 ns, clock hits posedge; regVal = 00000010
	#200 t_shft_in = 1; // @ 650 ns
	// @ 700 ns, clock hits posedge; regVal = 00000101
	#200 t_shft_in = 0; // @ 850 ns
	// @ 900 ns, clock hits posedge; regVal = 00001010
	#200 t_shft_in = 1; // @ 1050 ns
	// @ 1100 ns, clock hits posedge; regVal = 00010101
	#200 t_shft_in = 0; // @ 1250 ns
	// @ 1300 ns, clock hits posedge; regVal = 00101010
	#200 t_shft_in = 1; // @ 1450 ns
	// @ 1500 ns, clock hits posedge; regVal = 01010101
	#200 t_shft_in = 0; // @ 1650 ns
	// @ 1700 ns, clock hits posedge; regVal = 10101010
	#200 t_shft_in = 1; // @ 1850 ns
	// @ 1900 ns, clock hits posedge; regVal = 01010101
	#200 t_reset = 1; // @ 2050 ns
	// @ 2100 ns, clock hits posedge; regVal = 00000000
end

endmodule