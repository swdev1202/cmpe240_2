/*
 * file: shft_out_reg_testbench.v 
 * This is a testbench program to test the shift_out register (shft_out_reg.v)
 */

module shft_out_reg_testbench;

//inputs
reg[7:0] t_in;
reg t_clk;
reg t_reset;
reg t_load;
reg t_shift;

//outputs
wire t_shft_out;

// instantiation
shft_out_reg shifOutReg(.shft_out(t_shft_out),
						.in(t_in),
						.clk(t_clk),
						.reset(t_reset),
						.load(t_load),
						.shift(t_shift));
initial
begin
	// initialize
	t_clk = 0;
	t_reset = 1;
	t_load = 0;
	t_shift = 0;
end

always #100 t_clk = ~t_clk;

initial
begin
	#150 t_reset = 0; // @ 150 ns
	#100 t_load = 1; t_in = 8'b11001010; // @ 250 ns
	// @ 300 ns, clock hits posedge; regVal = 11001010
	#200 t_load = 0; t_shift = 1; // @ 450 ns
	// @ 500 ns, clock hits posedge; regVal = 10010100; out = 1
	#200 t_shift = 1; // @ 650 ns
	// @ 700 ns, clock hits posedge; regVal = 00101000; out = 1
	#200 t_shift = 1; // @ 850 ns
	// @ 900 ns, clock hits posedge; regVal = 01010000; out = 0
	#200 t_shift = 1; // @ 1050 ns
	// @ 1100 ns, clock hits posedge; regVal = 10100000; out = 0
	#200 t_shift = 1; // @ 1250 ns
	// @ 1300 ns, clock hits posedge; regVal = 01000000; out = 1
	#200 t_shift = 1; // @ 1450 ns
	// @ 1500 ns, clock hits posedge; regVal = 10000000; out = 0
	#200 t_shift = 1; // @ 1650 ns
	// @ 1700 ns, clock hits posedge; regVal = 00000000; out = 1
	#200 t_shift = 1; // @ 1850 ns
	// @ 1900 ns, clock hits posedge; regVal = 00000000; out = 0
	#200 t_in = 8'b11111111; t_load = 1; t_shift = 0;// @ 2050 ns
	// @ 2100 ns, clock hits posedge; regVal = 11111111; out = 1
	#200 t_reset = 1; // @2250 ns
	// @ 2300 ns, clock hits posedge; regVal = 00000000
end

endmodule