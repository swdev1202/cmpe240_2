/*
 * file: counter_3bit.v
 * This is a down 3bit counter
 */

module counter_3bit(out, clk, reset, count);

output[2:0] out;

input clk;
input reset;
input count;

reg[2:0] data;

assign out = data;

always@(negedge clk)
begin
	if(reset) begin
		data <= 3'b111;
	end
	else begin
		if(count) begin
			if(data > 3'b000) begin
				data <= data - 1;
			end
			else begin
				data <= 3'b111;
			end
		end
		else begin
			data <= 3'b111;
		end
	end
end

endmodule