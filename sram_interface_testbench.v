/*
 * file: sram_interface_testbench.v
 * This file is the testbench program to test sram_interface_testbench
 */

module sram_interface_testbench;

//inputs
reg t_sck;
reg t_sdi;
reg t_shft_addr;
reg t_shft_data;
reg t_wr_load;
reg t_enable;
reg t_shft_out;
reg t_out_load;
reg t_reset;
reg t_clk;
reg t_count;

//outputs
wire t_sdo;
wire t_wr_out;
wire[2:0] t_count_out;

// instantiation
sram_interface sramInterface(.sdo(t_sdo),
							 .wr_out(t_wr_out),
							 .count_out(t_count_out),
							 .sck(t_sck),
							 .sdi(t_sdi),
							 .shft_addr(t_shft_addr),
							 .shft_data(t_shft_data),
							 .wr_load(t_wr_load),
							 .enable(t_enable),
							 .shft_out(t_shft_out),
							 .out_load(t_out_load),
							 .reset(t_reset),
							 .clk(t_clk),
							 .count(t_count));
initial
begin
	//initialization
	t_sck = 0;
	t_shft_addr = 0;
	t_shft_data = 0;
	t_wr_load = 0;
	t_enable = 0;
	t_shft_out = 0;
	t_out_load = 0;
	t_reset = 1;
	t_clk = 0;
	t_count = 0;
end

always #400 t_sck = ~t_sck; // SCK is 8 times slower than SRAM clock
always #50 t_clk = ~t_clk;

initial
begin
	//write sequence
	#410 t_reset = 0; // @410ns
	#390 t_sdi = 1; // @800ns A[7]
	#10  t_shft_addr = 1; t_count = 1;// @810ns
	#790 t_sdi = 0; // @1600ns A[6]
	#800 t_sdi = 1; // @2400ns A[5]
	#800 t_sdi = 0; // @3200ns A[4]
	#800 t_sdi = 1; // @4000ns A[3]
	#800 t_sdi = 0; // @4800ns A[2]
	#800 t_sdi = 1; // @5600ns A[1]
	#800 t_sdi = 0; // @6400ns A[0]
	#10  t_count = 0; // @6410ns
	#790 t_sdi = 0; // @7200ns Write Signal
	#10	 t_shft_addr = 0; t_wr_load = 1; // @7210ns
	#790 t_sdi = 0; // @8000ns D[7]
	#10  t_wr_load = 0; t_shft_data = 1; t_count = 1; // @8010ns
	#790 t_sdi = 1; // @8800ns D[6]
	#800 t_sdi = 0; // @9600ns D[5]
	#800 t_sdi = 1; // @10400ns D[4]
	#800 t_sdi = 0; // @11200ns D[3]
	#800 t_sdi = 1; // @12000ns D[2]
	#800 t_sdi = 0; // @12800ns D[1]
	#800 t_sdi = 1; // @13600ns D[0]
	#10  t_count = 0; // @13610ns
	#800 t_shft_data = 0; t_enable = 1; // @14410ns write enable
	// end of write sequence, start the read sequence
	#790 t_sdi = 1; // @15200ns A[7]
	#10  t_enable = 0; t_shft_addr = 1; t_count = 1; // @15210ns
	#790 t_sdi = 0; // @16000ns
	#800 t_sdi = 1; // @16800ns
	#800 t_sdi = 0; // @17600ns
	#800 t_sdi = 1; // @18400ns
	#800 t_sdi = 0; // @19200ns
	#800 t_sdi = 1; // @20000ns
	#800 t_sdi = 0; // @20800ns
	#10  t_count = 0;
	#790 t_sdi = 1; // @21600ns Read Signal
	#10  t_shft_addr = 0; t_wr_load = 1; // @21610ns
	#800 t_wr_load = 0; t_enable = 1; t_out_load = 1; // @22410ns
	#800 t_enable = 0; t_out_load = 0; t_shft_out = 1; t_count = 1; // @23210ns
	#6400 $stop;
end

endmodule
	