/*
 * file: controller.v
 * This is a controller for the SRAM
 */
 
module controller(shft_addr, shft_data, wr_load, enable, shft_out, out_load, count,
					ss, sck, reset, wr_out, count_out);
output reg shft_addr;
output reg shft_data;
output reg wr_load;
output reg enable;
output reg shft_out;
output reg out_load;
output reg count;

input ss;
input sck;
input reset;
input wr_out;
input[2:0] count_out;

reg[3:0] state;

parameter idle=0, a7=1, a_6to1=2, a0=3, wr=4,
			w7=5, w_6to1=6, w0=7, w_enable=8,
			r_enable = 9, r7=10, r_6to1=11, r0 =12;

			
initial
begin
	state <= idle;
end

always@(state)
begin
	case(state)
		idle:begin
			shft_addr = 0;
			shft_data = 0;
			wr_load = 0;
			enable = 0;
			shft_out = 0;
			out_load = 0;
			count = 0;
		end
		
		a7:begin
			shft_addr = 1; // address reg shift starts
			shft_data = 0;
			wr_load = 0;
			enable = 0;
			shft_out = 0;
			out_load = 0;
			count = 1; // 3bit counter starts
		end
		
		a_6to1:begin
			shft_addr = 1; // address reg shift continues
			shft_data = 0;
			wr_load = 0;
			enable = 0;
			shft_out = 0;
			out_load = 0;
			count = 1; // 3bit counter continues
		end
		
		a0:begin
			shft_addr = 1; // address reg shift for the last
			shft_data = 0;
			wr_load = 0;
			enable = 0;
			shft_out = 0;
			out_load = 0;
			count = 0; // 3bit counter stops
		end
		
		wr:begin
			shft_addr = 0; // address reg shift stops
			shft_data = 0;
			wr_load = 1; // wr_load signal up
			enable = 0;
			shft_out = 0;
			out_load = 0;
			count = 0;
		end
		
		w7:begin
			shft_addr = 0;
			shft_data = 1; // data reg shift starts
			wr_load = 0; // wr_load signal down
			enable = 0;
			shft_out = 0;
			out_load = 0;
			count = 1; // 3bit counter starts
		end
		
		w_6to1:begin
			shft_addr = 0;
			shft_data = 1; // data reg shift continues
			wr_load = 0;
			enable = 0;
			shft_out = 0;
			out_load = 0;
			count = 1; // 3bit counter continues
		end
		
		w0:begin
			shft_addr = 0;
			shft_data = 1; // data reg shift for the last
			wr_load = 0;
			enable = 0;
			shft_out = 0;
			out_load = 0;
			count = 0; // 3bit counter stopss
		end
		
		w_enable:begin
			shft_addr = 0;
			shft_data = 0; // data reg shift stops
			wr_load = 0;
			enable = 1; // enable signal up
			shft_out = 0;
			out_load = 0;
			count = 0;
		end
		
		r_enable:begin
			shft_addr = 0;
			shft_data = 0; 
			wr_load = 0;
			enable = 1; // enable signal up 
			shft_out = 0;
			out_load = 1; // out_load signal up
			count = 0;
		end
		
		r7:begin
			shft_addr = 0;
			shft_data = 0;
			wr_load = 0;
			enable = 0; // enable signal down
			shft_out = 1; // data reg shift out starts
			out_load = 0; // out_load signal down
			count = 1; // 3bit counter starts	
		end
		
		r_6to1:begin
			shft_addr = 0;
			shft_data = 0;
			wr_load = 0;
			enable = 0;
			shft_out = 1; // data reg shift out continues
			out_load = 0;
			count = 1; // 3bit counter continues
		end
		
		r0:begin
			shft_addr = 0;
			shft_data = 0;
			wr_load = 0;
			enable = 0;
			shft_out = 1; // data reg shift for the last
			out_load = 0;
			count = 0; // 3bit counter stops
		end
		
		default:begin
			shft_addr = 0;
			shft_data = 0;
			wr_load = 0;
			enable = 0;
			shft_out = 0;
			out_load = 0;
			count = 0;
		end
		
	endcase
end

always@(negedge sck) // negative edge of SCK
begin
	if(reset)
		state <= idle;
	else
		case(state)
			idle:
				if(ss) begin // ss = 1; loop on idle
					state <= idle;
				end
				else begin // ss = 0; proceed
					state <= a7;
				end
			a7:
				state <= a_6to1;
			a_6to1:
				if(count_out > 3'b001) begin
					state <= a_6to1;
				end
				else begin
					state <= a0;
				end
			a0:
				state <= wr;
			wr:
				if(wr_out) begin //read
					state <= r_enable;
				end
				else begin //write
					state <= w7;
				end
			w7:
				state <= w_6to1;
			w_6to1:
				if(count_out > 3'b001) begin
					state <= w_6to1;
				end
				else begin
					state <= w0;
				end
			w0:
				state <= w_enable;
			w_enable:
				if(ss)	// ss is positive -> idle
					state <= idle;
				else	// continue and start from a7
					state <= a7;
			r_enable:
				state <= r7;
			r7:
				state <= r_6to1;
			r_6to1:
				if(count_out > 3'b001)
					state <= r_6to1;
				else
					state <= r0;
			r0:
				if(ss) begin	// ss is positive -> idle
					state <= idle;
				end
				else begin	// continue and start from a7
					state <= a7;
				end
		endcase
end

endmodule