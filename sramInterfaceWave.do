onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate -radix binary /sram_interface_testbench/t_sck
add wave -noupdate -radix binary /sram_interface_testbench/t_sdi
add wave -noupdate -radix binary /sram_interface_testbench/t_shft_addr
add wave -noupdate -radix binary /sram_interface_testbench/t_shft_data
add wave -noupdate -radix binary /sram_interface_testbench/t_wr_load
add wave -noupdate -radix binary /sram_interface_testbench/t_enable
add wave -noupdate -radix binary /sram_interface_testbench/t_shft_out
add wave -noupdate -radix binary /sram_interface_testbench/t_out_load
add wave -noupdate -radix binary /sram_interface_testbench/t_reset
add wave -noupdate -radix binary /sram_interface_testbench/t_clk
add wave -noupdate -radix binary /sram_interface_testbench/t_sdo
add wave -noupdate -radix binary /sram_interface_testbench/t_wr_out
add wave -noupdate /sram_interface_testbench/sramInterface/counter3bit/out
add wave -noupdate /sram_interface_testbench/sramInterface/counter3bit/count
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {218 ns} 0}
quietly wave cursor active 1
configure wave -namecolwidth 357
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 0
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ns
update
WaveRestoreZoom {0 ns} {23234 ns}
