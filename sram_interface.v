/*
 * file: sram_interface.v
 * This file is the whole datapath for the sram core
 */

module sram_interface(sdo, wr_out, count_out, sck, sdi, shft_addr, shft_data, wr_load, enable,
						shft_out, out_load, reset, clk, count);
output sdo;
output wr_out;
output[2:0] count_out;

input sck;
input sdi;
input shft_addr;
input shft_data;
input wr_load;
input enable;
input shft_out;
input out_load;
input reset;
input clk;
input count;

wire[7:0] sram_out;
wire[7:0] sram_in;
wire[7:0] sram_addr;

wire wEnable;
wire rEnable;

assign wEnable = ~(wr_out) & enable;
assign rEnable = wr_out & enable;
	
	
sram_core _sram_core(.dout(sram_out),
					 .din(sram_in),
					 .addr(sram_addr),
					 .clk(clk),
					 .we(wEnable),
					 .re(rEnable),
					 .reset(reset));

shft_in_reg addr_reg(.out(sram_addr),
					 .shft_in(sdi),
					 .clk(sck),
					 .reset(reset),
					 .shift(shft_addr));
					 
shft_in_reg data_reg(.out(sram_in),
					 .shft_in(sdi),
					 .clk(sck),
					 .reset(reset),
					 .shift(shft_data));
					 
shft_out_reg out_reg(.shft_out(sdo),
					 .in(sram_out),
					 .clk(sck),
					 .reset(reset),
					 .load(out_load),
					 .shift(shft_out));
					 
ff wr_ff(.q(wr_out),
		 .d(sdi),
		 .clk(sck),
		 .enable(wr_load),
		 .reset(reset));
		 
counter_3bit counter3bit(.out(count_out),
						 .clk(sck),
						 .reset(reset),
						 .count(count));
		 
endmodule