/*
 * file: controller_testbench.v
 * This file is the testbench program to test controller
 */

module controller_testbench;

//inputs
reg t_ss;
reg t_sck;
reg t_reset;
reg t_wr_out;
reg[2:0] t_count_out;

//outputs
wire t_shft_addr;
wire t_shft_data;
wire t_wr_load;
wire t_enable;
wire t_shft_out;
wire t_out_load;
wire t_count;

// instantiation
controller controllerInterface(.shft_addr(t_shft_addr),
							   .shft_data(t_shft_data),
							   .wr_load(t_wr_load),
							   .enable(t_enable),
							   .shft_out(t_shft_out),
							   .out_load(t_out_load),
							   .count(t_count),
							   .ss(t_ss),
							   .sck(t_sck),
							   .reset(t_reset),
							   .wr_out(t_wr_out),
							   .count_out(t_count_out));

initial
begin
	// initialization
	t_sck = 1;
	t_ss = 1;
	t_reset = 1;
end

always #400 t_sck = ~t_sck;

initial
begin
	#200 t_ss = 0; t_reset = 0;
	#9000 t_wr_out = 1;
	// 2016.11.23 checked how states proceed and it works fine
	// waiting to test with the final testbench
end

endmodule